package app.parqueo.application;

import app.parqueo.domain.model.ResponseInsertarZona;
import app.parqueo.domain.model.Zona;

public class ZonaValidator {

	public ResponseInsertarZona InsertarZona(Zona zona)
	{
		
		ResponseInsertarZona response = new ResponseInsertarZona();
		
		response.setCodError(null);
		response.setEstado(true);
		response.setMensaje(null);
		
		if(zona.getMuni_Id() == null) {
			response.setCodError(1);
			response.setEstado(false);
			response.setMensaje("Municipalidad vacio");
		}

		return response;
	}
}
