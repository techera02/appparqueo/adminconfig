package app.parqueo.application;

import java.util.List;

import org.springframework.stereotype.Service;

import app.parqueo.domain.model.ResponseZona;
import app.parqueo.domain.model.Zona;
import app.parqueo.domain.persistence_ports.ZonaPersistence;

@Service
public class ZonaService {

	ZonaPersistence zonaPersistence;
	
	public ZonaService(ZonaPersistence zonaPersistence)
	{
		this.zonaPersistence = zonaPersistence;
	}
	
	public List<ResponseZona> listarZonas(Integer idmunicipio, Integer pagenumber, Integer pagesize, Integer activo){
		return this.zonaPersistence.listarZonas(idmunicipio,pagenumber,pagesize,activo);
	}
	
	public Integer validarZona(Integer idzona, String descripcion, Integer idmuni) 
	{
        return this.zonaPersistence.validarZona(idzona, descripcion, idmuni);
    }
	
	public Integer insertarZona(Zona zona) 
	{
        return this.zonaPersistence.insertarZona(zona);
    }
	
	public Integer actualizarZona(Zona zona) {
		return this.zonaPersistence.actualizarZona(zona);
	}
	
	public Integer eliminarZona(Integer idzona) {
		return this.zonaPersistence.eliminarZona(idzona);
	}
	
	public Integer validarZonaDependencias(Integer idzona) {
		return this.zonaPersistence.validarZonaDependencias(idzona);
	}
}
