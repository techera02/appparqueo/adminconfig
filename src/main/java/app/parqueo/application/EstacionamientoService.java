package app.parqueo.application;

import java.util.List;

import org.springframework.stereotype.Service;

import app.parqueo.domain.model.Estacionamiento;
import app.parqueo.domain.model.ResponseEstacionamiento;
import app.parqueo.domain.persistence_ports.EstacionamientoPersistence;

@Service
public class EstacionamientoService {

	EstacionamientoPersistence estacionamientoPersistence;
	
	public EstacionamientoService(EstacionamientoPersistence estacionamientoPersistence)
	{
		this.estacionamientoPersistence = estacionamientoPersistence;
	}
	
	public Integer validarEstacionamiento(Integer idestacionamiento, String nombre, Integer idzona) 
	{
        return this.estacionamientoPersistence.validarEstacionamiento(idestacionamiento, nombre, idzona);
    }
	
	public Integer insertarEstacionamiento(Estacionamiento estacionamiento) 
	{
        return this.estacionamientoPersistence.insertarEstacionamiento(estacionamiento);
    }
	
	public Integer actualizarEstacionamiento(Estacionamiento estacionamiento) {
		return this.estacionamientoPersistence.actualizarEstacionamiento(estacionamiento);
	}
	
	public List<ResponseEstacionamiento> listarEstacionamientosPorZona(Integer idzona, Integer pagenumber, Integer pagesize,
			String nombre, String codigo)
	{
        return this.estacionamientoPersistence.listarEstacionamientosPorZona(idzona,pagenumber,pagesize,
        		nombre,codigo);
    }
	
	public Integer validarEstacionamientoExiste(Integer idestacionamiento) {
		return this.estacionamientoPersistence.validarEstacionamientoExiste(idestacionamiento);
	}
	
	public Integer eliminarEstacionamiento(Integer idestacionamiento) {
		return this.estacionamientoPersistence.eliminarEstacionamiento(idestacionamiento);
	}
	public Integer validarEstacionamientoCodigo(Integer indicador, String codigo) {
		return this.estacionamientoPersistence.validarEstacionamientoCodigo(indicador, codigo);
	}
}
