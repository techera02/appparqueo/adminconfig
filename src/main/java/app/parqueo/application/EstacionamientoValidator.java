package app.parqueo.application;

import app.parqueo.domain.model.Estacionamiento;
import app.parqueo.domain.model.ResponseInsertarEstacionamiento;

public class EstacionamientoValidator {

	public ResponseInsertarEstacionamiento InsertarEstacionamiento(Estacionamiento estacionamiento)
	{
		
		ResponseInsertarEstacionamiento response = new ResponseInsertarEstacionamiento();
		
		response.setCodError(null);
		response.setEstado(true);
		response.setMensaje(null);
		
		if(estacionamiento.getZona_Id() == null) {
			response.setCodError(1);
			response.setEstado(false);
			response.setMensaje("Zona vacio");
		}

		return response;
	}
}
