package app.parqueo.application;

import java.util.List;

import org.springframework.stereotype.Service;

import app.parqueo.domain.model.ResponseTarifa;
import app.parqueo.domain.model.Tarifa;
import app.parqueo.domain.persistence_ports.TarifaPersistence;

@Service
public class TarifaService {

	TarifaPersistence tarifaPersistence;
	
	public TarifaService(TarifaPersistence tarifaPersistence)
	{
		this.tarifaPersistence = tarifaPersistence;
	}
	
	public List<ResponseTarifa> listarTarifas(Integer pagenumber, Integer pagesize)
	{
		return this.tarifaPersistence.listarTarifas(pagenumber,pagesize);
	}
	
	public Integer validarTarifa(Integer idtarifa, Integer idmuni) 
	{
        return this.tarifaPersistence.validarTarifa(idtarifa, idmuni);
    }
	
	public Integer insertarTarifa(Tarifa tarifa) 
	{
        return this.tarifaPersistence.insertarTarifa(tarifa);
    }
	
	public Integer actualizarTarifa(Tarifa tarifa) {
		return this.tarifaPersistence.actualizarTarifa(tarifa);
	}
	
	public Integer validarTarifaExiste(Integer idtarifa) {
		return this.tarifaPersistence.validarTarifaExiste(idtarifa);
	}
	
	public Tarifa seleccionarTarifa(Integer idtarifa) {
		return this.tarifaPersistence.seleccionarTarifa(idtarifa);
	}
	
	public Integer eliminarTarifa(Integer idtarifa) {
		return this.tarifaPersistence.eliminarTarifa(idtarifa);
	}
}
