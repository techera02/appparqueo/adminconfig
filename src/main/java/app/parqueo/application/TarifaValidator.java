package app.parqueo.application;

import app.parqueo.domain.model.Tarifa;
import app.parqueo.domain.model.ResponseInsertarTarifa;

public class TarifaValidator {

	public ResponseInsertarTarifa InsertarTarifa(Tarifa tarifa)
	{
		
		ResponseInsertarTarifa response = new ResponseInsertarTarifa();
		
		response.setCodError(null);
		response.setEstado(true);
		response.setMensaje(null);
		
		if(tarifa.getMuni_Id() == null) {
			response.setCodError(1);
			response.setEstado(false);
			response.setMensaje("Municipalidad vacio");
		}

		return response;
	}
}
