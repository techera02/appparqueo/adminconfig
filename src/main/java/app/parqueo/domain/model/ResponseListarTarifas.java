package app.parqueo.domain.model;

import java.util.List;

public class ResponseListarTarifas extends ResponseError{

	private List<ResponseTarifa> lista;

	public List<ResponseTarifa> getLista() {
		return lista;
	}

	public void setLista(List<ResponseTarifa> lista) {
		this.lista = lista;
	}
}
