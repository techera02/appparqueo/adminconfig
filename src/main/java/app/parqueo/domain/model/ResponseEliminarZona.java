package app.parqueo.domain.model;

public class ResponseEliminarZona extends ResponseError{
	private boolean resultado;

	public boolean isResultado() {
		return resultado;
	}

	public void setResultado(boolean resultado) {
		this.resultado = resultado;
	}

}
