package app.parqueo.domain.model;

public class ResponseActualizarEstacionamiento extends ResponseError{

	private boolean resultado;

	public boolean getResultado() {
		return resultado;
	}

	public void setResultado(boolean resultado) {
		this.resultado = resultado;
	}
}
