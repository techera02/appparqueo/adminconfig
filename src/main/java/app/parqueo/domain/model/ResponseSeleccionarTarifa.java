package app.parqueo.domain.model;

public class ResponseSeleccionarTarifa extends ResponseError{

	private Tarifa tarifa;

	public Tarifa getTarifa() {
		return tarifa;
	}

	public void setTarifa(Tarifa tarifa) {
		this.tarifa = tarifa;
	}
}
