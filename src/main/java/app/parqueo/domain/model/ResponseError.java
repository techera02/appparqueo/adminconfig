package app.parqueo.domain.model;

public class ResponseError {
	private boolean estado;
	private String mensaje;
	private Integer codError;
	
	public boolean getEstado() 
	{
		return estado;
	}
	public void setEstado(boolean estado) 
	{
		this.estado = estado;
	}
	public String getMensaje() 
	{
		return mensaje;
	}
	public void setMensaje(String mensaje) 
	{
		this.mensaje = mensaje;
	}
	public Integer getCodError() 
	{
		return codError;
	}
	public void setCodError(Integer codError) 
	{
		this.codError = codError;
	}
}
