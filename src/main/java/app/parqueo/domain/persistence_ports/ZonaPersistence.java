package app.parqueo.domain.persistence_ports;

import java.util.List;

import app.parqueo.domain.model.ResponseZona;
import app.parqueo.domain.model.Zona;

public interface ZonaPersistence {

	List<ResponseZona> listarZonas(Integer idmunicipio, Integer pagenumber, Integer pagesize, Integer activo);
	
	Integer validarZona(Integer idzona, String descripcion, Integer idmuni);
	
	Integer insertarZona(Zona zona);
	
	Integer actualizarZona(Zona zona);
	
	Integer eliminarZona(Integer idzona);
	
	Integer validarZonaDependencias(Integer idzona);
}
