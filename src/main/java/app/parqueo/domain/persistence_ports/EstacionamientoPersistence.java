package app.parqueo.domain.persistence_ports;

import java.util.List;

import app.parqueo.domain.model.Estacionamiento;
import app.parqueo.domain.model.ResponseEstacionamiento;

public interface EstacionamientoPersistence {

	Integer validarEstacionamiento(Integer idestacionamiento, String nombre, Integer idzona);
	
	Integer insertarEstacionamiento(Estacionamiento estacionamiento);
	
	Integer actualizarEstacionamiento(Estacionamiento estacionamiento);
	
	List<ResponseEstacionamiento> listarEstacionamientosPorZona(Integer idzona, Integer pagenumber, Integer pagesize,
			String nombre, String codigo);
	
	Integer validarEstacionamientoExiste(Integer idestacionamiento);
	
	Integer eliminarEstacionamiento(Integer idestacionamiento);
	
	Integer validarEstacionamientoCodigo(Integer indicador, String codigo);

}
