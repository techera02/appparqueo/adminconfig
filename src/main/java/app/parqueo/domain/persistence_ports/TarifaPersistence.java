package app.parqueo.domain.persistence_ports;

import java.util.List;

import app.parqueo.domain.model.ResponseTarifa;
import app.parqueo.domain.model.Tarifa;

public interface TarifaPersistence {

	List<ResponseTarifa> listarTarifas(Integer pagenumber, Integer pagesize);
	
	Integer validarTarifa(Integer idtarifa, Integer idmuni);
	
	Integer insertarTarifa(Tarifa tarifa);
	
	Integer actualizarTarifa(Tarifa tarifa);
	
	Integer validarTarifaExiste(Integer idtarifa);
	
	Tarifa seleccionarTarifa(Integer idtarifa);
	
	Integer eliminarTarifa(Integer idtarifa);
}
