package app.parqueo.infrastructure.apis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app.parqueo.application.ZonaService;
import app.parqueo.application.ZonaValidator;
import app.parqueo.domain.model.ResponseActualizarZona;
import app.parqueo.domain.model.ResponseEliminarZona;
import app.parqueo.domain.model.ResponseInsertarZona;
import app.parqueo.domain.model.ResponseListarZonas;
import app.parqueo.domain.model.Zona;

@RestController
@RequestMapping("/Zona")
public class ZonaController {

	private final ZonaService zonaService;
	
	@Autowired
    public ZonaController(ZonaService zonaService) 
	{
        this.zonaService = zonaService;
    }
	
	@GetMapping("/ListarZonas/{idmunicipio}/{pagenumber}/{pagesize}/{activo}")
	public ResponseEntity<ResponseListarZonas> listarZonas(
			@PathVariable("idmunicipio")Integer idmunicipio, @PathVariable("pagenumber")Integer pagenumber,
			@PathVariable("pagesize")Integer pagesize,@PathVariable("activo")Integer activo)
	{
		ResponseListarZonas response = new ResponseListarZonas();
		response.setEstado(true);
		
		try {
			response.setLista(zonaService.listarZonas(idmunicipio,pagenumber,pagesize,activo));
		}catch(Exception e) {
			response.setEstado(true);
			response.setCodError(0);
			response.setMensaje("Error no controlado");
		}
		
		return ResponseEntity.ok(response);
	}
	
	@PostMapping("/InsertarZona")
    public ResponseEntity<ResponseInsertarZona> insertarZona
    (@RequestBody Zona zona)
	{  
		ResponseInsertarZona response = new ResponseInsertarZona();
		ZonaValidator zonaValidator = new ZonaValidator();
		
		response = zonaValidator.InsertarZona(zona);
		
		if(response.getEstado() == true) {
			
			try {
				Integer temp = this.zonaService.validarZona(0, zona.getZona_Descripcion(), zona.getMuni_Id());
				
				if(temp == 0) {
					this.zonaService.insertarZona(zona);
					response.setResultado(true);
					
				}else {
					response.setCodError(2);
					response.setMensaje("La zona ya existe");
					response.setEstado(false);
				}
				
			}catch(Exception e) {
				response.setCodError(0);
				response.setMensaje("Error no controlado");
				response.setEstado(false);
			}			
		}
		
		return ResponseEntity.ok(response);
	}
	
	@PutMapping("/ActualizarZona")
	public ResponseEntity<ResponseActualizarZona> actualizarZona
	(@RequestBody Zona zona)
	{ 
		ResponseActualizarZona response = new ResponseActualizarZona();
		response.setResultado(false);
		response.setEstado(true);

		try {
			
			Integer temp = this.zonaService.validarZona(zona.getZona_Id(), zona.getZona_Descripcion(), zona.getMuni_Id());
			
			if(temp == 0) {
				this.zonaService.actualizarZona(zona);
				response.setResultado(true);
					
			}else {
				response.setCodError(1);
				response.setMensaje("La zona ya existe");
				response.setEstado(false);
			}
			
		}catch(Exception e) {
			response.setCodError(0);
			response.setMensaje("Error no controlador");
			response.setEstado(false);
			
		}
		
		return ResponseEntity.ok(response);
	}
	
	@DeleteMapping("/EliminarZona/{idzona}")
	public ResponseEntity<ResponseEliminarZona> eliminarZona(@PathVariable("idzona")Integer idzona){
		ResponseEliminarZona response = new ResponseEliminarZona();
		response.setEstado(true);
		
		try {	
			Integer temp = this.zonaService.validarZonaDependencias(idzona);
			
			if(temp==0) {
				Integer temp2 = this.zonaService.eliminarZona(idzona);
				
				if(temp2 == 0) {
					response.setCodError(1);
					response.setMensaje("La zona no esta registrado");
					response.setEstado(false);
				}
			}else {
				response.setCodError(2);
				response.setMensaje("La zona tiene dependencias en estacionamientos");
				response.setEstado(false);
			}
			

		}catch(Exception e ) {
			response.setCodError(0);
			response.setMensaje("Error no controlado");
			response.setEstado(false);
		}
		return ResponseEntity.ok(response);
	}
}
