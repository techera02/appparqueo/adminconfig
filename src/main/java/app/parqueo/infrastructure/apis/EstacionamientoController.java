package app.parqueo.infrastructure.apis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app.parqueo.application.EstacionamientoService;
import app.parqueo.application.EstacionamientoValidator;
import app.parqueo.domain.model.Estacionamiento;
import app.parqueo.domain.model.ResponseActualizarEstacionamiento;
import app.parqueo.domain.model.ResponseEliminarEstacionamiento;
import app.parqueo.domain.model.ResponseInsertarEstacionamiento;
import app.parqueo.domain.model.ResponseListarEstacionamientosPorZona;

@RestController
@RequestMapping("/Estacionamiento")
public class EstacionamientoController {

	private final EstacionamientoService estacionamientoService;

	@Autowired
	public EstacionamientoController(EstacionamientoService estacionamientoService) {
		this.estacionamientoService = estacionamientoService;
	}

	@PostMapping("/InsertarEstacionamiento")
	public ResponseEntity<ResponseInsertarEstacionamiento> insertarEstacionamiento(
			@RequestBody Estacionamiento estacionamiento) {
		ResponseInsertarEstacionamiento response = new ResponseInsertarEstacionamiento();
		EstacionamientoValidator estacionamientoValidator = new EstacionamientoValidator();

		response = estacionamientoValidator.InsertarEstacionamiento(estacionamiento);

		if (response.getEstado() == true) {

			try {
				Integer temp = this.estacionamientoService.validarEstacionamiento(0, estacionamiento.getEsta_Nombre(),
						estacionamiento.getZona_Id());

				if (temp == 0) {

					Integer temp2 = this.estacionamientoService.validarEstacionamientoCodigo(0,
							estacionamiento.getEsta_Codigo());

					if (temp2 == 0) {
						this.estacionamientoService.insertarEstacionamiento(estacionamiento);
						response.setResultado(true);
					} else {
						response.setCodError(3);
						response.setMensaje("El codigo de estacionamiento ya existe");
						response.setEstado(false);
					}

				} else {
					response.setCodError(2);
					response.setMensaje("El estacionamiento ya existe");
					response.setEstado(false);
				}

			} catch (Exception e) {
				response.setCodError(0);
				response.setMensaje("Error no controlado");
				response.setEstado(false);
			}
		}

		return ResponseEntity.ok(response);
	}

	@PutMapping("/ActualizarEstacionamiento")
	public ResponseEntity<ResponseActualizarEstacionamiento> actualizarEstacionamiento(
			@RequestBody Estacionamiento estacionamiento) {
		ResponseActualizarEstacionamiento response = new ResponseActualizarEstacionamiento();
		response.setResultado(false);
		response.setEstado(true);

		try {

			Integer temp = this.estacionamientoService.validarEstacionamiento(estacionamiento.getEsta_Id(),
					estacionamiento.getEsta_Nombre(), estacionamiento.getZona_Id());

			if (temp == 0) {
				Integer temp2 = this.estacionamientoService.validarEstacionamientoCodigo(1,
						estacionamiento.getEsta_Codigo());

				if (temp2 == 1 || temp2 == 0) {
					this.estacionamientoService.actualizarEstacionamiento(estacionamiento);
					response.setResultado(true);
				} else {
					response.setCodError(3);
					response.setMensaje("El codigo de estacionamiento ya existe");
					response.setEstado(false);
				}

			} else {
				response.setCodError(1);
				response.setMensaje("El estacionamiento ya existe");
				response.setEstado(false);
			}

		} catch (Exception e) {
			response.setCodError(0);
			response.setMensaje("Error no controlado");
			response.setEstado(false);

		}

		return ResponseEntity.ok(response);
	}

	@GetMapping("/ListarEstacionamientosPorZona/{idzona}/{pagenumber}/{pagesize}/{nombre}/{indicenombre}/{codigo}/{indicecodigo}")
	public ResponseEntity<ResponseListarEstacionamientosPorZona> listarEstacionamientosPorZona(
			@PathVariable("idzona") Integer idzona, @PathVariable("pagenumber") Integer pagenumber,
			@PathVariable("pagesize") Integer pagesize, @PathVariable("nombre") String nombre,
			@PathVariable("indicenombre") Integer indicenombre, @PathVariable("codigo") String codigo,
			@PathVariable("indicecodigo") Integer indicecodigo) {

		ResponseListarEstacionamientosPorZona response = new ResponseListarEstacionamientosPorZona();
		response.setEstado(true);

		if (indicenombre == 0) {
			nombre = "";
		}
		if (indicecodigo == 0) {
			codigo = "";
		}

		try {
			response.setLista(this.estacionamientoService.listarEstacionamientosPorZona(idzona, pagenumber, pagesize,
					nombre, codigo));
		} catch (Exception e) {
			response.setCodError(0);
			response.setEstado(false);
			response.setMensaje("Error no controlado");
		}

		return ResponseEntity.ok(response);
	}

	@DeleteMapping("/EliminarEstacionamiento/{idestacionamiento}")
	public ResponseEntity<ResponseEliminarEstacionamiento> eliminarEstacionamiento(
			@PathVariable("idestacionamiento") Integer idestacionamiento) {
		ResponseEliminarEstacionamiento response = new ResponseEliminarEstacionamiento();
		response.setEstado(true);

		try {

			Integer temp = this.estacionamientoService.validarEstacionamientoExiste(idestacionamiento);

			if (temp == 1) {
				this.estacionamientoService.eliminarEstacionamiento(idestacionamiento);
				response.setResultado(true);
			} else {
				response.setCodError(1);
				response.setMensaje("Estacionamiento no registrado");
				response.setEstado(false);
			}

		} catch (Exception e) {
			response.setCodError(0);
			response.setMensaje("Error no controlado");
			response.setEstado(false);
		}
		return ResponseEntity.ok(response);
	}
}
