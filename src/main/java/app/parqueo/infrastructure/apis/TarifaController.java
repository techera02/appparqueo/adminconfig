package app.parqueo.infrastructure.apis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app.parqueo.application.TarifaService;
import app.parqueo.application.TarifaValidator;
import app.parqueo.domain.model.ResponseActualizarTarifa;
import app.parqueo.domain.model.ResponseEliminarTarifa;
import app.parqueo.domain.model.ResponseInsertarTarifa;
import app.parqueo.domain.model.ResponseListarTarifas;
import app.parqueo.domain.model.ResponseSeleccionarTarifa;
import app.parqueo.domain.model.Tarifa;

@RestController
@RequestMapping("/Tarifa")
public class TarifaController {

	private final TarifaService tarifaService;
	
	@Autowired
    public TarifaController(TarifaService tarifaService) 
	{
        this.tarifaService = tarifaService;
    }
	
	@GetMapping("/ListarTarifas/{pagenumber}/{pagesize}")
	public ResponseEntity<ResponseListarTarifas> listarTarifas(
			@PathVariable("pagenumber")Integer pagenumber, @PathVariable("pagesize")Integer pagesize)
	{
		ResponseListarTarifas response = new ResponseListarTarifas();
		response.setEstado(true);
		
		try {
			response.setLista(tarifaService.listarTarifas(pagenumber,pagesize));
		}catch(Exception e) {
			response.setEstado(false);
			response.setCodError(0);
			response.setMensaje("Error no controlado");
		}
		
		return ResponseEntity.ok(response);
	}
	
	@PostMapping("/InsertarTarifa")
    public ResponseEntity<ResponseInsertarTarifa> insertarTarifa
    (@RequestBody Tarifa tarifa)
	{  
		ResponseInsertarTarifa response = new ResponseInsertarTarifa();
		TarifaValidator tarifaValidator = new TarifaValidator();
		
		response = tarifaValidator.InsertarTarifa(tarifa);
		
		if(response.getEstado() == true) {
			
			try {
				Integer temp = this.tarifaService.validarTarifa(0, tarifa.getMuni_Id());
				
				if(temp == 0) {
					this.tarifaService.insertarTarifa(tarifa);
					response.setResultado(true);
					
				}else {
					response.setCodError(2);
					response.setMensaje("La tarifa ya existe");
					response.setEstado(false);
				}
				
			}catch(Exception e) {
				response.setCodError(0);
				response.setMensaje("Error no controlado");
				response.setEstado(false);
			}			
		}
		
		return ResponseEntity.ok(response);
	}
	
	@PutMapping("/ActualizarTarifa")
	public ResponseEntity<ResponseActualizarTarifa> actualizarTarifa
	(@RequestBody Tarifa tarifa)
	{ 
		ResponseActualizarTarifa response = new ResponseActualizarTarifa();
		response.setResultado(false);
		response.setEstado(true);

		try {
			
			Integer temp = this.tarifaService.validarTarifa(tarifa.getTari_Id(), tarifa.getMuni_Id());
			
			if(temp == 0) {
				this.tarifaService.actualizarTarifa(tarifa);
				response.setResultado(true);
					
			}else {
				response.setCodError(1);
				response.setMensaje("La tarifa ya existe");
				response.setEstado(false);
			}
			
		}catch(Exception e) {
			response.setCodError(0);
			response.setMensaje("Error no controlado");
			response.setEstado(false);
			
		}
		
		return ResponseEntity.ok(response);
	}
	
	@GetMapping("/SeleccionarTarifa/{idtarifa}")
	public ResponseEntity<ResponseSeleccionarTarifa> seleccionarTarifa(@PathVariable("idtarifa")Integer idtarifa) {
		ResponseSeleccionarTarifa response = new ResponseSeleccionarTarifa();
		response.setEstado(true);
		
		try {
			
			Integer temp = this.tarifaService.validarTarifaExiste(idtarifa);
			
			if(temp == 1) {
				response.setTarifa(this.tarifaService.seleccionarTarifa(idtarifa));
			}else {
				response.setCodError(1);
				response.setMensaje("Tarifa no registrado");
				response.setEstado(false);
			}			
			
		}catch(Exception e ) {
			response.setCodError(0);
			response.setMensaje("Error no controlado");
			response.setEstado(false);
		}
		return ResponseEntity.ok(response);
	}
	
	@DeleteMapping("/EliminarTarifa/{idtarifa}")
	public ResponseEntity<ResponseEliminarTarifa> eliminarTarifa(@PathVariable("idtarifa")Integer idtarifa) {
		ResponseEliminarTarifa response = new ResponseEliminarTarifa();
		response.setEstado(true);
		
		try {
			
			Integer temp = this.tarifaService.validarTarifaExiste(idtarifa);
			
			if(temp == 1) {
				this.tarifaService.eliminarTarifa(idtarifa);
				response.setResultado(true);
			}else {
				response.setCodError(1);
				response.setMensaje("Tarifa no registrado");
				response.setEstado(false);
			}
			
			
		}catch(Exception e ) {
			response.setCodError(0);
			response.setMensaje("Error no controlado");
			response.setEstado(false);
		}
		return ResponseEntity.ok(response);
	}
}
