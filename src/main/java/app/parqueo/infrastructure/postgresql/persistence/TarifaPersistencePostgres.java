package app.parqueo.infrastructure.postgresql.persistence;

import java.util.ArrayList;
import java.util.List;
import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import app.parqueo.domain.model.ResponseTarifa;
import app.parqueo.domain.model.Tarifa;
import app.parqueo.domain.persistence_ports.TarifaPersistence;
import app.parqueo.infrastructure.postgresql.repositories.TarifaRepository;

@Repository("TarifaPersistence")
public class TarifaPersistencePostgres implements TarifaPersistence{

	private final TarifaRepository tarifaRepository;
	
    @Autowired
    public TarifaPersistencePostgres(TarifaRepository tarifaRepository) 
    {
        this.tarifaRepository = tarifaRepository;
    }
    
    @Override
	public List<ResponseTarifa> listarTarifas(Integer pagenumber, Integer pagesize) {
		List<Object[]> list = this.tarifaRepository.listarTarifas(pagenumber,pagesize);
		List<ResponseTarifa> listaTarifa = new ArrayList<ResponseTarifa>();
		
		for(int x=0; x < list.size(); x++) {
			ResponseTarifa entidad = new ResponseTarifa();
			entidad.setRowNumber(Integer.parseInt(list.get(x)[0].toString()));
			entidad.setRowCount(Integer.parseInt(list.get(x)[1].toString()));
			entidad.setPageCount(Integer.parseInt(list.get(x)[2].toString()));
			entidad.setPageIndex(Integer.parseInt(list.get(x)[3].toString()));
			entidad.setTari_Id(Integer.parseInt(list.get(x)[4].toString()));
			entidad.setMuni_Id(Integer.parseInt(list.get(x)[5].toString()));
			entidad.setTari_Monto(new BigDecimal(list.get(x)[6].toString()));
			entidad.setTari_UsuarioCreacion(Integer.parseInt(list.get(x)[7].toString()));
			entidad.setTari_UsuarioEdicion(Integer.parseInt(list.get(x)[8].toString()));
			
			listaTarifa.add(entidad);
		}
		return listaTarifa;
	}
	
	@Override
	public Integer validarTarifa(Integer idtarifa, Integer idmuni)
	{
		return this.tarifaRepository.validarTarifa(idtarifa, idmuni);
	}
	
	@Override
	public Integer insertarTarifa(Tarifa tarifa) 
	{
		return this.tarifaRepository.insertarTarifa(tarifa.getMuni_Id(), tarifa.getTari_Monto(), tarifa.getTari_UsuarioCreacion());
	}
	
	@Override
	public Integer actualizarTarifa(Tarifa tarifa) 
	{
		return this.tarifaRepository.actualizarTarifa(tarifa.getTari_Id(), tarifa.getMuni_Id(), tarifa.getTari_Monto(),
				tarifa.getTari_UsuarioEdicion());
	}
	
	@Override
	public Integer validarTarifaExiste(Integer idtarifa) {
		return this.tarifaRepository.validarTarifaExiste(idtarifa);
	}
	
	@Override
	public Tarifa seleccionarTarifa(Integer idtarifa) {
		return this.tarifaRepository.seleccionarTarifa(idtarifa).toTarifa();
	}
	
	@Override
	public Integer eliminarTarifa(Integer idtarifa) {
		return this.tarifaRepository.eliminarTarifa(idtarifa);
	}
}
