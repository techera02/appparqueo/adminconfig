package app.parqueo.infrastructure.postgresql.persistence;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import app.parqueo.domain.model.Estacionamiento;
import app.parqueo.domain.model.ResponseEstacionamiento;
import app.parqueo.domain.persistence_ports.EstacionamientoPersistence;
import app.parqueo.infrastructure.postgresql.repositories.EstacionamientoRepository;

@Repository("EstacionamientoPersistence")
public class EstacionamientoPersistencePostgres implements EstacionamientoPersistence{

	private final EstacionamientoRepository estacionamientoRepository;
	
	@Autowired
    public EstacionamientoPersistencePostgres(EstacionamientoRepository estacionamientoRepository) 
    {
        this.estacionamientoRepository = estacionamientoRepository;
    }
	
	@Override
	public Integer validarEstacionamiento(Integer idestacionamiento, String nombre, Integer idzona) 
	{
		return this.estacionamientoRepository.validarEstacionamiento(idestacionamiento, nombre, idzona);
	}
	
	@Override
	public Integer insertarEstacionamiento(Estacionamiento estacionamiento)
	{
		return this.estacionamientoRepository.insertarEstacionamiento(estacionamiento.getZona_Id(), estacionamiento.getEsta_Nombre(), 
				estacionamiento.getEsta_UsuarioCreacion(), estacionamiento.getEsta_Geometry(), estacionamiento.getEsta_Codigo(),
				estacionamiento.getEsta_Disponible(), estacionamiento.getEsta_Discapacitado());
	}
	
	@Override
	public Integer actualizarEstacionamiento(Estacionamiento estacionamiento)
	{
		return this.estacionamientoRepository.actualizarEstacionamiento(estacionamiento.getEsta_Id(), estacionamiento.getZona_Id(),
				estacionamiento.getEsta_Nombre(), estacionamiento.getEsta_UsuarioEdicion(), estacionamiento.getEsta_Geometry(),
				estacionamiento.getEsta_Codigo(),estacionamiento.getEsta_Disponible(), estacionamiento.getEsta_Discapacitado());
	}
	
	@Override
	public List<ResponseEstacionamiento> listarEstacionamientosPorZona(Integer idzona, Integer pagenumber, Integer pagesize,
			String nombre, String codigo) {
		List<Object[]> list = this.estacionamientoRepository.listarEstacionamientosPorZona(idzona,pagenumber,pagesize,
				nombre,codigo);
		List<ResponseEstacionamiento> listaEstacionamiento = new ArrayList<ResponseEstacionamiento>();
		
		for(int x = 0;x<list.size();x++) {
			ResponseEstacionamiento entity = new ResponseEstacionamiento();
			entity.setRowNumber(Integer.parseInt(list.get(x)[0].toString()));
			entity.setRowCount(Integer.parseInt(list.get(x)[1].toString()));
			entity.setPageCount(Integer.parseInt(list.get(x)[2].toString()));
			entity.setPageIndex(Integer.parseInt(list.get(x)[3].toString()));
			entity.setEsta_Id(Integer.parseInt(list.get(x)[4].toString()));
			entity.setZona_Id(Integer.parseInt(list.get(x)[5].toString()));
			entity.setEsta_Nombre(list.get(x)[6].toString());
			entity.setEsta_Geometry(list.get(x)[7].toString());
			entity.setEsta_Codigo(list.get(x)[8].toString());
			entity.setEsta_Disponible(Boolean.parseBoolean(list.get(x)[9].toString()));
			entity.setEsta_Discapacitado(Boolean.parseBoolean(list.get(x)[10].toString()));
			listaEstacionamiento.add(entity);
		}
		return listaEstacionamiento;
	}
	
	@Override
	public Integer validarEstacionamientoExiste(Integer idestacionamiento) {
		return this.estacionamientoRepository.validarEstacionamientoExiste(idestacionamiento);
	}
	
	@Override
	public Integer eliminarEstacionamiento(Integer idestacionamiento) {
		return this.estacionamientoRepository.eliminarEstacionamiento(idestacionamiento);
	}

	@Override
	public Integer validarEstacionamientoCodigo(Integer indicador, String codigo) {
		return this.estacionamientoRepository.validarEstacionamientoCodigo(indicador, codigo);
	}
}
