package app.parqueo.infrastructure.postgresql.persistence;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import app.parqueo.domain.model.ResponseZona;
import app.parqueo.domain.model.Zona;
import app.parqueo.domain.persistence_ports.ZonaPersistence;
import app.parqueo.infrastructure.postgresql.repositories.ZonaRepository;

@Repository("ZonaPersistence")
public class ZonaPersistencePostgres implements ZonaPersistence{

	private final ZonaRepository zonaRepository;
	
    @Autowired
    public ZonaPersistencePostgres(ZonaRepository zonaRepository) 
    {
        this.zonaRepository = zonaRepository;
    }
    
	@Override
	public List<ResponseZona> listarZonas(Integer idmunicipio, Integer pagenumber, Integer pagesize, Integer activo) {
		List<Object[]> list = this.zonaRepository.listarZonas(idmunicipio,pagenumber,pagesize,activo);
		List<ResponseZona> listaZona = new ArrayList<ResponseZona>();
		
		for(int x=0; x < list.size(); x++) {
			ResponseZona entidad = new ResponseZona();
			entidad.setRowNumber(Integer.parseInt(list.get(x)[0].toString()));
			entidad.setRowCount(Integer.parseInt(list.get(x)[1].toString()));
			entidad.setPageCount(Integer.parseInt(list.get(x)[2].toString()));
			entidad.setPageIndex(Integer.parseInt(list.get(x)[3].toString()));			
			entidad.setZona_Id(Integer.parseInt(list.get(x)[4].toString()));
			entidad.setMuni_Nombre(list.get(x)[5].toString());
			entidad.setZona_Descripcion(list.get(x)[6].toString());
			entidad.setMuni_Id(Integer.parseInt(list.get(x)[7].toString()));
			entidad.setZona_Activo(Boolean.parseBoolean(list.get(x)[8].toString()));
			entidad.setZona_UsuarioCreacion(Integer.parseInt(list.get(x)[9].toString()));
			entidad.setZona_UsuarioEdicion(Integer.parseInt(list.get(x)[10].toString()));
			entidad.setZona_Geometry(list.get(x)[11].toString());
			
			listaZona.add(entidad);
		}
		return listaZona;
	}
	
	@Override
	public Integer validarZona(Integer idzona, String descripcion, Integer idmuni) 
	{
		return this.zonaRepository.validarZona(idzona, descripcion, idmuni);
	}
	
	@Override
	public Integer insertarZona(Zona zona) 
	{
		return this.zonaRepository.insertarZona(zona.getZona_Descripcion(), zona.getMuni_Id(), 
				zona.getZona_Activo(), zona.getZona_UsuarioCreacion(), zona.getZona_Geometry());
	}
	
	@Override
	public Integer actualizarZona(Zona zona) 
	{
		return this.zonaRepository.actualizarZona(zona.getZona_Id(), zona.getZona_Descripcion(), zona.getMuni_Id(), 
				zona.getZona_Activo(), zona.getZona_UsuarioEdicion(), zona.getZona_Geometry());
	}

	@Override
	public Integer eliminarZona(Integer idzona) {
		return this.zonaRepository.eliminarZona(idzona);
	}

	@Override
	public Integer validarZonaDependencias(Integer idzona) {
		return this.zonaRepository.validarZonaDependencias(idzona);
	}
}
