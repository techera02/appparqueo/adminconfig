package app.parqueo.infrastructure.postgresql.repositories;

import java.math.BigDecimal;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import app.parqueo.infrastructure.postgresql.entities.TarifaEntity;

public interface TarifaRepository extends JpaRepository<TarifaEntity, Integer>{

	@Query(nativeQuery = true, value = "SELECT * FROM \"UFN_ListarTarifas\"(:pagenumber,:pagesize)")
	List<Object[]> listarTarifas(@Param("pagenumber") Integer pagenumber, @Param("pagesize") Integer pagesize);
	
	@Query(value = "SELECT \"UFN_ValidarTarifa\"(:idtarifa,:idmuni)", nativeQuery = true)
	Integer validarTarifa(@Param("idtarifa") Integer idtarifa, @Param("idmuni") Integer idmuni);
	
	@Query(value = "CALL \"USP_Tarifa_INS\"(:idmuni,:monto,:usuariocreacion)", nativeQuery = true)
	@Modifying(clearAutomatically = true)
	@Transactional
	Integer insertarTarifa(@Param("idmuni") Integer idmuni, @Param("monto") BigDecimal monto,
			@Param("usuariocreacion") Integer usuariocreacion);
	
	@Query(value = "CALL \"USP_Tarifa_UPD\"(:idtarifa,:idmuni,:monto,:usuarioedicion)", nativeQuery = true)
	@Modifying(clearAutomatically = true)
	@Transactional
	Integer actualizarTarifa(@Param("idtarifa") Integer idtarifa, @Param("idmuni") Integer idmuni, 
			@Param("monto") BigDecimal monto, @Param("usuarioedicion") Integer usuarioedicion);
	
	@Query(value = "SELECT * FROM \"UFN_ValidarTarifaExiste\"(:idtarifa)", nativeQuery = true)
	Integer validarTarifaExiste(@Param("idtarifa") Integer idtarifa);
	
	@Query(value = "SELECT * FROM \"UFN_SeleccionarTarifa\"(:idtarifa)", nativeQuery = true)
	TarifaEntity seleccionarTarifa(@Param("idtarifa") Integer idtarifa);
	
	@Query(value = "CALL \"USP_Tarifa_DEL\"(:idtarifa)", nativeQuery = true)
	@Modifying(clearAutomatically = true)
	@Transactional
	Integer eliminarTarifa(@Param("idtarifa") Integer idtarifa);
}
