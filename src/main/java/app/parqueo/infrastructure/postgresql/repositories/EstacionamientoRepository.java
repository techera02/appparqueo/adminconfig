package app.parqueo.infrastructure.postgresql.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import app.parqueo.infrastructure.postgresql.entities.EstacionamientoEntity;

public interface EstacionamientoRepository extends JpaRepository<EstacionamientoEntity, Integer>{

	@Query(value = "SELECT \"UFN_ValidarEstacionamiento\"(:idestacionamiento,:nombre,:idzona)", nativeQuery = true)
	Integer validarEstacionamiento(@Param("idestacionamiento") Integer idestacionamiento, @Param("nombre") String nombre,
			@Param("idzona") Integer idzona);
	
	@Query(value = "CALL \"USP_Estacionamiento_INS\"(:idzona,:nombre,:usuariocreacion,:geometry,:codigo,:disponible,:discapacitado)", nativeQuery = true)
	@Modifying(clearAutomatically = true)
	@Transactional
	Integer insertarEstacionamiento(
			@Param("idzona") Integer idzona, @Param("nombre") String nombre, @Param("usuariocreacion") Integer usuariocreacion,
			@Param("geometry") String geometry, @Param("codigo") String codigo, @Param("disponible") boolean disponible,
			@Param("discapacitado") boolean discapacitado);
	
	@Query(value = "CALL \"USP_Estacionamiento_UPD\"(:idestacionamiento,:idzona,:nombre,:usuarioedicion,:geometry,:codigo,:disponible,:discapacitado)", nativeQuery = true)
	@Modifying(clearAutomatically = true)
	@Transactional
	Integer actualizarEstacionamiento(
			@Param("idestacionamiento") Integer idestacionamiento,@Param("idzona") Integer idzona, @Param("nombre") String nombre,
			@Param("usuarioedicion") Integer usuarioedicion, @Param("geometry") String geometry, @Param("codigo") String codigo,
			@Param("disponible") boolean disponible, @Param("discapacitado") boolean discapacitado);
	
	@Query(value = "SELECT * FROM \"UFN_ListarEstacionamientosPorZonaPaginado\"(:idzona,:pagenumber,:pagesize,:nombre,:codigo)", 
			nativeQuery = true)
	List<Object[]> listarEstacionamientosPorZona(
			@Param("idzona") Integer idzona, 
			@Param("pagenumber") Integer pagenumber,
			@Param("pagesize") Integer pagesize,
			@Param("nombre") String nombre,
			@Param("codigo") String codigo
			);
	
	@Query(value = "SELECT * FROM \"UFN_ValidarEstacionamientoExiste\"(:idestacionamiento)", nativeQuery = true)
	Integer validarEstacionamientoExiste(@Param("idestacionamiento") Integer idestacionamiento);
	
	@Query(value = "CALL \"USP_Estacionamiento_DEL\"(:idestacionamiento)", nativeQuery = true)
	@Modifying(clearAutomatically = true)
	@Transactional
	Integer eliminarEstacionamiento(@Param("idestacionamiento") Integer idestacionamiento);
	
	@Query(value = "SELECT * FROM \"UFN_ValidarEstacionamientoCodigo\"(:indicador,:codigo)", nativeQuery = true)
	Integer validarEstacionamientoCodigo(@Param("indicador") Integer indicador, @Param("codigo") String codigo);
	
}
