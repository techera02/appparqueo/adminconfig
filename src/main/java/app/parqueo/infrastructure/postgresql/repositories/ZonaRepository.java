package app.parqueo.infrastructure.postgresql.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import app.parqueo.infrastructure.postgresql.entities.ZonaEntity;

public interface ZonaRepository extends JpaRepository<ZonaEntity, Integer>{

	@Query(nativeQuery = true, value = "SELECT * FROM \"UFN_ListarZonas\"(:idmunicipio,:pagenumber,:pagesize, :activo)")
	List<Object[]> listarZonas(
			@Param("idmunicipio") Integer idmunicipio,
			@Param("pagenumber") Integer pagenumber,
			@Param("pagesize") Integer pagesize,
			@Param("activo") Integer activo
			);
	
	@Query(value = "SELECT \"UFN_ValidarZona\"(:idzona,:descripcion,:idmuni)", nativeQuery = true)
	Integer validarZona(@Param("idzona") Integer idzona, @Param("descripcion") String descripcion, @Param("idmuni") Integer idmuni);
	
	@Query(value = "CALL \"USP_Zona_INS\"(:descripcion,:idmuni,:activo,:usuariocreacion,:geometry)", nativeQuery = true)
	@Modifying(clearAutomatically = true)
	@Transactional
	Integer insertarZona(
			@Param("descripcion") String descripcion, @Param("idmuni") Integer idmuni, @Param("activo") boolean activo,
			@Param("usuariocreacion") Integer usuariocreacion, @Param("geometry") String geometry);
	
	
	@Query(value = "CALL \"USP_Zona_UPD\"(:idzona,:descripcion,:idmuni,:activo,:usuarioedicion,:geometry)", nativeQuery = true)
	@Modifying(clearAutomatically = true)
	@Transactional
	Integer actualizarZona(
			@Param("idzona") Integer idzona, @Param("descripcion") String descripcion, @Param("idmuni") Integer idmuni,
			@Param("activo") boolean activo, @Param("usuarioedicion") Integer usuarioedicion, @Param("geometry") String geometry
			);
	
	@Query(value = "SELECT \"UFN_EliminarZona\"(:idzona)", nativeQuery = true)
	Integer eliminarZona(@Param("idzona") Integer idzona);
	
	@Query(value = "SELECT \"UFN_ValidarZonaDenpendencias\"(:idzona)", nativeQuery = true)
	Integer validarZonaDependencias(@Param("idzona") Integer idzona);
	
}
