package app.parqueo.infrastructure.postgresql.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import app.parqueo.domain.model.Zona;

@Entity
@Table(name="\"Zona\"")
public class ZonaEntity {

	@Id
	private Integer Zona_Id;
	private String Zona_Descripcion;
	private Integer Muni_Id;
	private boolean Zona_Activo;
	private Integer Zona_UsuarioCreacion;
	private Integer Zona_UsuarioEdicion;
	private String Zona_Geometry;
	
	public Integer getZona_Id() {
		return Zona_Id;
	}
	public void setZona_Id(Integer zona_Id) {
		Zona_Id = zona_Id;
	}
	public String getZona_Descripcion() {
		return Zona_Descripcion;
	}
	public void setZona_Descripcion(String zona_Descripcion) {
		Zona_Descripcion = zona_Descripcion;
	}
	public Integer getMuni_Id() {
		return Muni_Id;
	}
	public void setMuni_Id(Integer muni_Id) {
		Muni_Id = muni_Id;
	}
	public boolean getZona_Activo() {
		return Zona_Activo;
	}
	public void setZona_Activo(boolean zona_Activo) {
		Zona_Activo = zona_Activo;
	}
	public Integer getZona_UsuarioCreacion() {
		return Zona_UsuarioCreacion;
	}
	public void setZona_UsuarioCreacion(Integer zona_UsuarioCreacion) {
		Zona_UsuarioCreacion = zona_UsuarioCreacion;
	}
	public Integer getZona_UsuarioEdicion() {
		return Zona_UsuarioEdicion;
	}
	public void setZona_UsuarioEdicion(Integer zona_UsuarioEdicion) {
		Zona_UsuarioEdicion = zona_UsuarioEdicion;
	}	
	public String getZona_Geometry() {
		return Zona_Geometry;
	}
	public void setZona_Geometry(String zona_Geometry) {
		Zona_Geometry = zona_Geometry;
	}

	public Zona toZona() {
		Zona zona = new Zona();
		BeanUtils.copyProperties(this, zona);
		return zona;
	}
}
