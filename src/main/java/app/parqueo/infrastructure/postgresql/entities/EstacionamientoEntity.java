package app.parqueo.infrastructure.postgresql.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="\"Estacionamiento\"")
public class EstacionamientoEntity {

	@Id
	private Integer Esta_Id;
	private Integer Zona_Id;
	private String Esta_Nombre;
	private Integer Esta_UsuarioCreacion;
	private Integer Esta_UsuarioEdicion;
	private String Esta_Geometry;
	private String Esta_Codigo;
	private boolean Esta_Disponible;
	private boolean Esta_Discapacitado;
	
	public Integer getEsta_Id() {
		return Esta_Id;
	}
	public void setEsta_Id(Integer esta_Id) {
		Esta_Id = esta_Id;
	}
	public Integer getZona_Id() {
		return Zona_Id;
	}
	public void setZona_Id(Integer zona_Id) {
		Zona_Id = zona_Id;
	}
	public String getEsta_Nombre() {
		return Esta_Nombre;
	}
	public void setEsta_Nombre(String esta_Nombre) {
		Esta_Nombre = esta_Nombre;
	}
	public Integer getEsta_UsuarioCreacion() {
		return Esta_UsuarioCreacion;
	}
	public void setEsta_UsuarioCreacion(Integer esta_UsuarioCreacion) {
		Esta_UsuarioCreacion = esta_UsuarioCreacion;
	}
	public Integer getEsta_UsuarioEdicion() {
		return Esta_UsuarioEdicion;
	}
	public void setEsta_UsuarioEdicion(Integer esta_UsuarioEdicion) {
		Esta_UsuarioEdicion = esta_UsuarioEdicion;
	}
	public String getEsta_Geometry() {
		return Esta_Geometry;
	}
	public void setEsta_Geometry(String esta_Geometry) {
		Esta_Geometry = esta_Geometry;
	}
	public String getEsta_Codigo() {
		return Esta_Codigo;
	}
	public void setEsta_Codigo(String esta_Codigo) {
		Esta_Codigo = esta_Codigo;
	}
	public boolean getEsta_Disponible() {
		return Esta_Disponible;
	}
	public void setEsta_Disponible(boolean esta_Disponible) {
		Esta_Disponible = esta_Disponible;
	}
	public boolean getEsta_Discapacitado() {
		return Esta_Discapacitado;
	}
	public void setEsta_Discapacitado(boolean esta_Discapacitado) {
		Esta_Discapacitado = esta_Discapacitado;
	}
}
