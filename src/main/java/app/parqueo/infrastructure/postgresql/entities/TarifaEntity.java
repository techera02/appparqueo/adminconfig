package app.parqueo.infrastructure.postgresql.entities;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import app.parqueo.domain.model.Tarifa;

@Entity
@Table(name="\"Tarifa\"")
public class TarifaEntity {

	@Id
	private Integer Tari_Id;
	private Integer Muni_Id;
	private BigDecimal Tari_Monto;
	private Integer Tari_UsuarioCreacion;
	private Integer Tari_UsuarioEdicion;
	
	public Integer getTari_Id() {
		return Tari_Id;
	}
	public void setTari_Id(Integer tari_Id) {
		Tari_Id = tari_Id;
	}
	public Integer getMuni_Id() {
		return Muni_Id;
	}
	public void setMuni_Id(Integer muni_Id) {
		Muni_Id = muni_Id;
	}
	public BigDecimal getTari_Monto() {
		return Tari_Monto;
	}
	public void setTari_Monto(BigDecimal tari_Monto) {
		Tari_Monto = tari_Monto;
	}
	public Integer getTari_UsuarioCreacion() {
		return Tari_UsuarioCreacion;
	}
	public void setTari_UsuarioCreacion(Integer tari_UsuarioCreacion) {
		Tari_UsuarioCreacion = tari_UsuarioCreacion;
	}
	public Integer getTari_UsuarioEdicion() {
		return Tari_UsuarioEdicion;
	}
	public void setTari_UsuarioEdicion(Integer tari_UsuarioEdicion) {
		Tari_UsuarioEdicion = tari_UsuarioEdicion;
	}
	
	public Tarifa toTarifa() {
		Tarifa tarifa = new Tarifa();
		BeanUtils.copyProperties(this, tarifa);
		return tarifa;
	}
}
